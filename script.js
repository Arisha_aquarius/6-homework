// Теоретичні питання
// 1.Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
// Для того,что б отгородить символы,которые могут являться частью синтаксиса JS для использования специального символа как обычного  
// 2.Які засоби оголошення функцій ви знаєте?
// Function declaration, function expression, named function expression

// Що таке hoisting, як він працює для змінних та функцій?

//механизм, при котором переменные и объявления функции поднимаются вверх по своей области видимости перед выполнением кода
// var можно использовать до обьявления переменной, но с let и const так нельзя. и function declaration так работает,что можно вызывать перед обьявлением функции в коде




function createNewUser() {
  const firstName = prompt("What is your name?");
  const lastName = prompt("What is your surname?");
  const birthDate = prompt("What is your date of birth (dd.mm.yyyy)?");

  const newUser = {
    firstName,
    lastName,
    birthDate,
    getAge() {
      const today = new Date();
      const birthDateArray = this.birthDate.split(".");
      console.log(birthDateArray);
      const birthYear = parseInt(birthDateArray[2], 10);
      console.log(birthYear);
      const birthMonth = parseInt(birthDateArray[1], 10) - 1;
      const birthDay = parseInt(birthDateArray[0], 10);
      const age = today.getFullYear() - birthYear;

      return age;
    },
    getPassword() {
      const firstLetterUpper = this.firstName[0].toUpperCase();
      const lastNameLower = this.lastName.toLowerCase();
      const birthYear = this.birthDate.split(".")[2];
      return `${firstLetterUpper}${lastNameLower}${birthYear}`;
    },
    getLogin() {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },
  };

  return newUser;
}

const user = createNewUser();
console.log(`Password: ${user.getPassword()}`);
console.log(`Age: ${user.getAge()}`);




















